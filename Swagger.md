# Swagger

#### 学习目标：

- 了解Swagger的作用和概念
- 了解前后端分离
- 在SpringBoot中集成Swagger



## Swagger简介

#### 前后端分离

Vue + SpringBoot



后端时代：前端只用管理静态页面：html ，写完交给后端。模板引擎 JSP ==> 后端主力



前后端分离时代：

- 后端：后端控制层，服务层，数据访问层【后端团队】
- 前端：前端控制层，视图层【前端团队】
  - 伪造后端数据，json。已经存在了，不需要后端，前端工程依旧能跑
- 前后端如何交互？ ==> API
- 前后端相对独立，松耦合
- 甚至可以部署在不同的服务器上



产生一个问题：

- 前后端集成联调，前端人员和后端人员无法做到 “及时协调，尽早解决”。最终导致问题集中爆发

解决方案：

- 首先制定schema（计划的提纲），实时更新最新的API，降低集成的风险；
- 早些年：制定Word计划文档；
- 前后端分离：
  - 前端测试后端接口：postman
  - 后端提供接口，需要实时更新最新的消息及改动！



### Swagger

- 号称世界上最流行的Api框架
- RestFul Api 文档在线自动生成工具 -> **Api文档与Api定义同步更新**
- 直接运行，可以在线测试API接口；
- 支持多种语言：（JAVA，PHP）



官网：https://swagger.io/



在项目使用Swagger需要Springbox；

- swagger2
- ui



## SpringBoot集成Swagger

1. 新建一个SpringBoot项目（web）
2. 导入相关依赖

```xml
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger2 -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
</dependency>
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
```

3. 编写一个Hello工程

4. 配置Swagger ==> Config

   ```java
   // @Configuration配置到配置里面
   @Configuration
   // 开启Swagger
   @EnableSwagger2
   public class SwaggerConfig {
   }
   ```

5. 测试运行

![image-20200715133907288](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200715133907288.png)



### 配置Swagger

```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    // 配置了swagger的docket的bean实例
    @Bean
    public Docket docket() {
        return  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo());
    }

    // 配置swagger信息
    private ApiInfo apiInfo() {
        Contact contact = new Contact("陈烨庆", "", "123@123.com");
        return new ApiInfo(
                "cyq的SwaggerApi文档",
                "学习Swagger中...",
                "v 1.0",
                "urn:tos",
                 contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                 new ArrayList());
    }

}
```

### Swagger配置扫描接口

Docket.select()

```java
// 配置了swagger的docket的bean实例
    @Bean
    public Docket docket() {
        return  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                // RequestHandlerSelectors， 配置要扫描接口的方式
                // basePackage() 指定要扫描的包
                // any() 扫描全部
                // none() 不扫描
                // withClassAnotation 扫描类上的注解，参数时一个注解的反射对象
                // withMethodAnotation 扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.study.swaggerdemo.controller"))
            	// 过滤什么路径
                .paths(PathSelectors.ant("/study/**"))
                .build();
    }
```



配置是否启动Swagger

```java
@Bean
public Docket docket() {
    return  new Docket(DocumentationType.SWAGGER_2)
        // enable 是否启动Swagger， 如果为false， 则Swagger 不能在浏览器中访问
        .enable(false);
```



问题：如果只希望Swagger在生产环境中使用，在发布的时候不使用？

- 判断环境是不是生产环境，flag = false
- 注入enable(flag)

```java
// 配置了swagger的docket的bean实例
    @Bean
    public Docket docket(Environment environment) {

        // 设置要显示的Swagger环境
        Profiles profiles = Profiles.of("dev", "test");

        // 通过environment 判断是否处在自己设定的环境当中
        boolean flag = environment.acceptsProfiles(profiles);


        return  new Docket(DocumentationType.SWAGGER_2)
                // enable 是否启动Swagger， 如果为false， 则Swagger 不能在浏览器中访问
                .enable(flag)
```



配置API文档的分组

```java
.groupName("张三")
```

如何配置多个分组： 配置多个docket实例即可。

```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {


    @Bean
    public Docket docket2(Environment environment) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("李四");
    }
    @Bean
    public Docket docket3(Environment environment) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("王五");
    }

```



实体类配置：

```java
@ApiModel("用户实体类")
public class User {
    @ApiModelProperty("用户名")
    public String username;
    @ApiModelProperty("密码")
    public String password;
}
```

Controller配置：

```java
@Api(tags = "Controller控制器")
@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello() {
        return "Hello World!";
    }

    @ApiOperation("user")
    @GetMapping("/user")
    public User user() {
        return new User("张三","pwd");
    }

    @ApiOperation("hello2方法")
    @GetMapping("/hello2")
    public String hello2(@ApiParam("用户名") String username) {
        return "Hello " + username;
    }

    @ApiOperation("post测试")
    @PostMapping("/postt")
    public User postt(@ApiParam("User对象") User user) {
        return user;
    }
}
```



### 总结：

- 可以通过Swagger给一些比较难理解的属性或者接口，增加注释信息
- 接口文档实时更新
- 可以在线测试

#### Swagger是个优秀的工具

【注意点】在正式发布的时候，关闭Swagger！！！出于安全考虑，而且节省运行的内存。

