# Redis



## 如何连接其他服务器



```shell
redis-cli -h 192.168.0.10 -p 6379
```

# Nosql概述

### 为什么要用Nosql

什么是NoSQL

> ## 单机MySQL的年代！

### 	APP >> DAL >> Mysql

![image-20200716112200230](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716112200230.png)

90年代，一个基本的网站访问量一般不会太大，单个数据库完全足够！

那个时候，更多的去使用静态页面 Html， 服务器根本没有太大的压力。

这种情况下：整个网站的瓶颈是什么？

1. **数据量如果太大**，一个机器放不下！
2. 数据的索引  300万就一定要建立索引（B + Tree）
3. 访问量（读写混合），一个服务器承受不了

只要开始出现以上的3中情况之一，那么你就必须要晋级！



> ## Memcached（缓存） + MySQL + 垂直拆分

网站80%的情况都是在读，每次都要去查询数据库就非常的麻烦！所以我们希望减轻数据的压力，我们可以使用缓存来保证效率！

发展过程： 优化数据结构和索引 -> 文件缓存（IO） -> Memcached（当时最热门的技术！）



> ## 分库分表 + 水平拆分 + MySQL集群

技术和业务在发展的同时，对人的要求也越来越高！

**本质：数据库（读 + 写）**

早年：MyISAM：表锁（100万）十分影响效率！

现在：InnoDB：行锁

慢慢的就开始分库分表来解决写的压力！MySQL在那个年代推出了表分区！这个并没有多少公司使用！

MySQL的集群，很好的满足了那个年代的需求！

![image-20200716113103789](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716113103789.png)



> ## 如今最近的年代

技术爆炸

2010（按键手机） -- 2020：十年之间，世界已经发生了翻天覆地的变化：（定位，也是一种数据，音乐，热榜）

MySQL等关系型数据库就不够用了！数据量很多，变化很大！

MySQL有的使用它来存储一些比较大的文件，博客，图片！数据库表很大，效率就低了！如果有一种数据库来专门处理这种数据

Mysql的压力就变得十分小（研究如何处理这些问题！）大数据的IO压力下，表几乎没法更大！ 1亿数据，要加列

**灰度发布**

> 目前一个基本的互联网项目

![image-20200716124020808](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716124020808.png)

为什么要用NoSQL，用户的个人信息，社区网络，地理位置，用户自己产生的数据，用户日志等等爆发式增长！

这时候我们就需要使用NoSQL数据库，NoSQL可以很好的处理以上的情况！



# 什么是NoSQL

> NoSQL

NoSQL = Not Only SQL（不仅仅是SQL）

关系型数据库：表格，行和列（POI）

泛指非关系型数据库，随着WEB2.0互联网的诞生！传统的关系型数据库很难对付WEB2.0时代！尤其是超大规模的高并发的社区（早年有个职业是站长）！暴露出来很多难以克服的问题，NoSQL在当今大数据环境下发展的十分迅速，Redis是发展最快而且使我们当下必须要掌握的技术！

很多的数据类型用户的个人信息，社区网络，地理位置，用户自己产生的数据，用户日志。这些数据类型的存储不需要一个固定的格式！不需要多余的操作，就可以横向扩展的！ Map<String, Object>	用键值对来控制！

> NoSQL特点

解耦！

1. 方便扩展（数据之间没有关系，很好扩展）
2. 大数据量高性能（Redis一秒 写8W次，读取11W次，NoSQL的缓存记录级，是一种细粒度的缓存，性能会比较高！性能比较高！）
3. 数据类型是多样型的（不需要事先设计数据库！随取随用！如果是数据库量十分大的表，很多人就无法设计了！）
4. 传统RDBMS和NoSQL

```
传统的RDBMS（关系型数据）
- 结构化组织
- SQL
- 数据和关系都存储在单独的表中 row和col
- 操作数据，数据定义语言
- 严格的一致性
- 基础的事务
- ...
```

```
NoSQL
- 不仅仅是数据
- 没有固定的查询语言
- 键值对存储，列存储，文档存储，图形数据库（社交关系）
- 最终一致性
- CAP定力	和	BASE	（异地多活！）
- 高性能，高可用，高可扩
- ...
```



> 了解3V + 3高

大数据时代的3V：主要是描述问题的

​		海量Velume

​		多样Variety

​		实时Velocity(4G)

大数据时代的3高：主要是对程序的要求

​		高并发

​		高可扩（随时水平拆分，机器不够了，可以扩展来解决）

​		高性能（保证用户体验和性能）



真正在公司中的实践：NoSQL+RDBMS一起使用才是最强的，阿里巴巴的架构演进！



**技术没有高低之分，就看你如何使用（思维的提高）**



# 阿里巴巴演进分析

思考问题，这么多东西难道都是在一个数据库中的吗？











开源才是技术的王道

敏捷开发、极限编程

任何一家互联网公司，都不可能只是简简单单的让用户能用就好了

大量公司做的都是相同的业务：（竞品协议）

随着这样的竞争，业务越来越完善，然后对于开发者的要求也是越来越高





架构师：没有什么是加一层解决不了的

```bash
#  1、 商品的基本信息
	名称、价格、商家信息：
		关系型数据库就可以解决了！MySQL / Oracle	（淘宝早年就去IOE了！ - 王坚，推荐文章：阿里云的这群疯子：40分钟重要！）
		淘宝内部的MySQL 不是大家用的MySQL
		
# 2、 商品的描述、评论（文字比较多）
	文档型数据库：MongoDB
	
# 3、 图片
	分布式文件系统 FastDFS
	- 淘宝自己的TFS
	- Google的GFS
	- Hadoop HDFS
	- 阿里云 OSS
	
# 4、 商品的关键字（搜索）
	- 搜索引擎	solr	elasticsearch
	- ISearch：多隆（多去了解一下这些技术大佬！）
	
# 5、 商品热门的波段信息
	- 内存数据库
	- Redis、Tair、Memache
	
# 6、 商品的交易、外部的支付接口
	- 三方应用
```

一个简单的网页背后的技术一定不是大家想的那么简单！

大型互联网应用问题：

- 数据类型太多了
- 数据源繁多，经常重构
- 数据要改造，大面积改造

解决问题：

- 统一数据服务层UDSL



以上都是NoSQL入门概述，提高知识，了解历史，了解大厂的工作内容





# NoSQL的四大分类

**KV键值：**

- 新浪：**Redis**
- 美团：Redis + Tair
- 阿里、百度：Redis + memecache

**文档型数据库（bson格式和json一样）**

- **MoongoDB**(一般必须要掌握)
  - MongoDB是一个基于分布式文件存储的数据库，C++编写，主要用来处理大量的文档
  - MongoDB是一个介于关系型数据库和非关系型数据库中间的产品！MongoDB是非关系型数据库中功能最丰富，最像关系型数据库的！
  - ConthDB

**列存储数据库**

- **HBase**
- 分布式文件系统

**图关系数据库**

![myfriends](https://greatpowerlaw.files.wordpress.com/2012/10/myfriends_thumb.jpg?w=655&h=525)

- 它不是存图形，放的是关系，比如：朋友圈社交网络，广告推荐！
- **Neo4j**，InfoGrid；



![image-20200716142905922](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716142905922.png)

敬畏之心可以使人进步。宇宙。科幻。





# Redis入门

## 概述

> Redis是什么？

Redis（**Re**mote **Di**ctionary **S**erver），远程字典服务，是一个**开源**的使用ANSI **[C语言](https://baike.baidu.com/item/C语言)**编写、支持网络、可基于内存亦可**持久化的日志型**、Key-Value[数据库](https://baike.baidu.com/item/数据库/103728)，并提供多种语言的API。

免费和开源！是当下最热门的NoSQL技术之一！也被人们称之为结构化数据库！

redis会周期性的把更新的数据写入磁盘或者把修改操作写入追加的记录文件，并且在此基础上实现了master-slave(主从)同步。

> Redis能干嘛？

1. 内存存储、持久化，内存中是断电即失、所以说持久化很重要（rdb、aof）
2. 效率高，可以用于高速缓存
3. 发布订阅系统
4. 地图信息分析
5. 计时器、计数器（浏览量）
6. ......

> 特性

1. 多样的数据类型
2. 持久化
3. 集群
4. 事务



> 学习中需要用到的东西

1. 狂神的公众号：狂神说
2. 官网：https://redis.io/
3. http://www.redis.cn/topics/introduction
4. 中文网：http://www.redis.cn/
5. 下载地址：通过官网下载



Windows在Github上停更了

**Redis推荐在Linux上搭建，基于Linux学习**

## Window安装

1. 下载安装包：https://github.com/microsoftarchive/redis/releases/tag/win-3.2.100
2. 下载完毕得到解压包
3. 解压。非常小，5M
4. 开启Redis，双击运行服务即可
5. 使用Redis客户端来连接Redis

```
ping	测试连通
set name zhangsan	set基本值	key value
get name			get key 获取值
```



## Linux安装

1. 下载安装包https://redis.io/download

2. 解压Redis，程序放到opt目录下

   ![image-20200716155147906](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716155147906.png)

3. 进入解压后的文件可以看到redis的配置文件

   ![image-20200716155443963](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716155443963.png)

4. 基本的环静安装

```
yum install gcc-c++
    报错添加：
        yum -y install centos-release-scl
        yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils
        scl enable devtoolset-9 bash
make
make install
```

![image-20200716170003485](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716170003485.png)

5. Redis的默认安装路径`/usr/local/bin`

   ![image-20200716170323298](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716170323298.png)

6. 将Redis配置文件，复制到我们当前目录下

![image-20200716170622104](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716170622104.png)

7. redis默认不是后台启动的，修改配置文件！

   ![image-20200716171127647](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716171127647.png)

8. 启动Redis服务，**前面加上./**

   ![image-20200716171518406](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716171518406.png)

9. 使用redis-cli进行连接测试！

   ![image-20200716171912674](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716171912674.png)

10. 查看redis的进程是否开启`ps -ef|grep redis`

11. 如何关闭redis服务呢？`shutdown`

    ![image-20200716172214315](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716172214315.png)



## 性能测试

**redis-benchmark**是一个压力测试工具

官方自带的性能测试工具！

redis-benchmark命令参数！

![image-20200716173947018](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716173947018.png)



```
# 测试：100个并发连接	100000请求
redis-benchmark -h localhost -p 6379 -c 100 -n 100000
```

![image-20200716175251660](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716175251660.png)

![image-20200716175340073](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200716175340073.png)



## 基础的知识





redis默认有16个数据库



默认使用第0个

可以使用select 进行切换数据库

`select 3`切换数据库

`DBSIZE`查看DB大小

`keys *`查看全部的key

`FLUSHDB`清空当前数据库

`FLUSHALL`清空全部数据库



为什么Redis是6379

粉丝效应，手机9键



> Redis是单线程的

Redis是很快的，官方表示，Redis是基于内存操作的，CPU不是Redis的性能瓶颈，Redis的瓶颈是根据机器的内存和网络带宽，既然可以使用单线程可以实现，就使用了单线程



Redis是C语言写的，官方提供的数据位10W+的QPS，这个不比同样是使用Key-Value的MemeCache差！

**Redis 为什么单线程还这么快**

1. 误区1：高性能的服务器一定是多线程的？
2. 误区2：多线程（CPU上下文会切换）一定比单线程效率高！

先去CPU>内存>硬盘的速度要有所了解

核心：Redis是将所有的数据全部放在内存中的，所以说使用单线程去操作效率就是最高的，多线程（CPU上下文会切换：耗时的操作），对于内存系统来说，如果没有上下文切换，效率就是最高的！多次读写都是在一个CPU上的，在内存情况下，这个就是最佳的方案



# 五大数据类型

Redis 是一个开源（BSD许可）的，内存中的数据结构存储系统，它可以用作数据库、缓存和消息中间件。 它支持多种类型的数据结构，如 [字符串（strings）](http://www.redis.cn/topics/data-types-intro.html#strings)， [散列（hashes）](http://www.redis.cn/topics/data-types-intro.html#hashes)， [列表（lists）](http://www.redis.cn/topics/data-types-intro.html#lists)， [集合（sets）](http://www.redis.cn/topics/data-types-intro.html#sets)， [有序集合（sorted sets）](http://www.redis.cn/topics/data-types-intro.html#sorted-sets) 与范围查询， [bitmaps](http://www.redis.cn/topics/data-types-intro.html#bitmaps)， [hyperloglogs](http://www.redis.cn/topics/data-types-intro.html#hyperloglogs) 和 [地理空间（geospatial）](http://www.redis.cn/commands/geoadd.html) 索引半径查询。 Redis 内置了 [复制（replication）](http://www.redis.cn/topics/replication.html)，[LUA脚本（Lua scripting）](http://www.redis.cn/commands/eval.html)， [LRU驱动事件（LRU eviction）](http://www.redis.cn/topics/lru-cache.html)，[事务（transactions）](http://www.redis.cn/topics/transactions.html) 和不同级别的 [磁盘持久化（persistence）](http://www.redis.cn/topics/persistence.html)， 并通过 [Redis哨兵（Sentinel）](http://www.redis.cn/topics/sentinel.html)和自动 [分区（Cluster）](http://www.redis.cn/topics/cluster-tutorial.html)提供高可用性（high availability）。

> 所有的命令都要全部记住！！！后面使用springboot，kedis，所有的方法就是这些命令！
>
> 单点登录

## Redis-Key

```
keys *				查看所有key
set name zhangsan 	 set key
exists name			判断当前的key是否存在
move key			移除当前的key
expire key			设置到期时间，单位是秒				
ttl key				查看当前key的剩余时间
```



## String(字符串)

90%的java程序员使用redis只会使用一个String类型！

```bash
set key1 v1		设置值

get key1		获取值

keys *			获取所有值

exists			判断某一个key是否存在

append key1 "v1"	追加字符串，如果当前key不存在，就相当于set key

strlen key1		获取字符串的长度
############################################################################
i++

incr views	自增1

decr views	自减1

incrby views 10	自增10

decrby views 10	自减10
############################################################################
字符串范围	range


set key1 "Hello, World!"

getrange key1 0 3	从第0个开始，取到第三个

getrange key1 0 -1	从第0个开始，取到最后一个

替换字符串	

setrange key2 1 xx	替换指定位置开始的字符串

############################################################################
setex (set with expire)		#设置过期时间
setnx (set if not exists)	#不存在在设置	（分布式锁中会常常使用！）



setex key3 10 "hello"		设置key3的值为hello，10秒后过期

setnx mykey "redis"			如果mykey不存在，创建mykey

setnx mykey "MongoDB"		如果mykey存在，创建失败

############################################################################
mset 多次set
mget 多次get



mset k1 v1 k2 v2 k3 v3		同时设置多个值

mget k1 k2 k3				同时获取多个值

msetnx k1 v1 k4 v4			msetnx 是一个原子性的操作，要么一起成功，要么一起失败

############################################################################
#对象


set user:1 {name:zhangsan, age:3}		设置一个user：1对象，值为 json 字符来保存一个对象

这里的key是一个巧妙的设计    >>>>   user:{id}:{filed}			#如此设计在Redis中是完全OK的！

############################################################################
getset



getset db redis			#如果不存在值，则返回 nil

getset db mongodb		#如果存在值，获取原来的值，并设置新的值
```

数据结构是相通的。

String类似的使用场景：value除了是我们的字符串还可以是我们的数字！

- 计数器，文章的阅读数量
- 统计多单位数量
- 粉丝数
- 对象缓存存储
- .......

## List（列表）

基本的数据类型，列表



在redis里面，我们可以吧list玩成，栈、队列、阻塞队列

所有的list命令都是用L开头的

```
###########################################################################
添加和查看
LPUSH list one			#将一个值，或多个值插入到列表头部

Lpush list two			#同上

LRANGE list 0 -1		#获取list中的值

RPUSH list zero			#将一个值，插入到列表尾部

###########################################################################
添加删除列表元素

LPOP list				#移除list的第一个元素

RPOP list				#移除list的最后一个元素

Lrem list 2 three			#移除2个three元素

###########################################################################
获取下标

Lindex list 1			#通过下标获得list中的某一个值

###########################################################################
获取长度

Llen list				#获取list的长度

###########################################################################

trim 修剪：list  截断

ltrim list 1 2			通过下标截取指定的长度，这个list已经被改变了，截断了只剩下截取的元素

###########################################################################

rpoplpush list otherlist		#移除最后一个元素，将它移动到新的列表

###########################################################################

Lset list 0 item			#强列表中指定下标的值替换为另外一个值，更新操作

如果不存在列表，Lset就会报错

如果存在，更新当前下标的值

###########################################################################
Linsert list BEFORE/AFTER item value		#将某个元素添加到item的前面或者后面
```

> 小结

- 它实际上是一个链表，before Node after，left 、right都可以插入值
- 如果key不存在，创建新的链表
- 如果key存在，新增内容
- 如果溢出了所有值，空链表，也代表不存在
- 在两边插入或改动值效率最高！中间元素，相对来说效率会低一点

消息排队、消息队列（Lpush、Rpop）、栈（Lpush、Lpop）

## Set（集合）

set中的值是不能重复的！

```
###########################################################################
添加、查看和删除

sadd myset hello				#set集合中添加元素

sadd myset zhangsan

srem myset zhangsan				#set集合中移除元素

sadd myset lisi

smembers myset					#查看指定set的所有值

sismember myset hello			#判断某一个值是不是在set集合中

###########################################################################

scard myset						#获取set集合中的个数

###########################################################################

set 无序不重复集合， 抽随机

srandmember myset				#随机抽选出一个元素

srandmember myset 2				#随机抽出指定个数的元素

spop myset						#随机移除元素

###########################################################################

将一个指定的值，移动到另一个集合中

smove myset myset2 hello		

###########################################################################

微博、B站，共同关注（交集）

sdiff myset1 myset2				两个set的差异		显示myset1中没有的值
sinter myset1 myset2			两个set的交集		共同好友的实现
sunion myset1 myset2			两个set的并集
```

微博，A用户将所有关注的人放在一个set集合中，将他的粉丝也放在一个集合中

共同关注，共同爱好，二度好友（六度分割理论），推荐好友！

## Hash（哈希）

Map集合，Key-Map，这个值是一个Map集合

本质和String类型没有太大区别，还是一个简单的Key-Value

```
#############################################################################

添加

hset myhash field1 zhangsan				#set一个具体的Key-Value

hmset myhash field1 value1 field2 value2	#set多个Key-Value


########################################

查看

hget myhash field1						#获取一个的Key-Value

hmget myhash field1 field2				#获取多个Key-Value

hgetall myhash							#获取全部的数据

#####################################
删除

hdel myhash field1						#删除hash指定的key字段

##########################################################################

获取长度

hlen myhash								#获取hash的字段数量

#####################################

判断哈希中的指定字段是否存在

hexists myhash field1

#####################################

获得所有的字段

hkeys myhash


获得所有的值

hvals myhash

#####################################

incr		decr

hincrby myhash field9 10				#指定增量

hsetnx myhash field8 value				#如果不存在则可以设置

hsetnx myhash field8 value2				#如果存在，则不能设置

#####################################

hset user:1 name zhangsan
```

hash变更的数据 user，name，age，尤其是用户信息之类的，经常变动的信息！

hash更适合于对象的存储，String更适合字符串存储





## Zset(有序集合)

在set的基础上，增加了一个值，	set k1 v1 > zset k1 score1 v1

```bash
##########################################################################

添加值

zadd myset 1 one						# 添加一个值
		
zadd myset 2 two 3 three				# 添加多个值

移除

zrem myset one							#移除one

查看

zrange myset 0 -1						#查看myset的值

##########################################################################

排序如何实现

zadd salary 2500 zhangsan

zadd salary 5000 lisi

zadd salary 500 wangwu								#添加3个用户

zrangebyscore salary -inf +inf						#升序

zrangebyscore salary -inf +inf withscores			#带score升序

zrevrengebyscore salary +inf -inf					#降序

zrevrengebyscore salary +inf -inf withscores		#带score降序

##########################################################################

获取集合中的个数

zcard salary

zcount salary 0 3000							#查看区间内的值有几个

```

官方文档

案例思路：

set 排序	存储班级成绩表	工资表排序！

普通消息：1	重要消息：2	带权重进行判断！

排行榜应用实现。Top N 测试

# 三种特殊数据类型









# 事务

Redis 事务本质：一组命令的集合！在事务执行过程中，会按照顺序执行

一次性，顺序性，排他性！执行一系列的命令

```
------ 队列 
set
set
set
执行
-------
```

**Redis事务没有隔离级别的概念**

所有的命令在事务中，并没有直接被执行！只有发起执行命令的时候才会执行！Exec

Redis单条命令是保证原子性的，但是事务不保证原子性

Redis的事务：

- 开启事务（multi）
- 命令入队（）
- 执行事务（）

> 正常开启事务

```bash
multi										# 开启事务
set k1 v1									# 命令入队
set k2 v2
get k2
set k3 v3
exec										# 执行事务
```

> 放弃事务

```bash
multi										# 开启事务
set k1 v1
set k2 v2
set k9 v9
discard										# 取消事务
get k9										# 事务队列中的命令不会被执行
```

> 编译型异常（代码有问题，命令有错），事务中的命令都不会被执行！

```bash
multi					# 开启事务
set k1 v1
set k2 v2
set k3 v3
getset k3				# 此句命令错误
set k4 v4
set k5 v5
exec					# 执行事务
get k2					# nil
get k5					# nil
# 所有的命令都不会被执行
```

> 运行时异常（1/0，下标异常）语法型错误，那么执行命令的时候，其他命令是可以正常执行的，错误命令抛出异常

```bash
set k1 "v1"					# 设置一个准备自增的字符
multi			
incr k1						# 这句在运行时会报错
set k2 v2			
set k3 v3
get k3
exec
```

![image-20200720110617107](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200720110617107.png)







> 监控。Watch

悲观锁：

- 很悲观，认为什么时候都会出问题，无论做什么都会加锁

乐观锁：

- 很乐观，认为什么时候都不会出现问题，所以不会上锁！更新数据的时候去判断一下，在此期间是否有人修改过这个数据。
- 获取version
- 更新的时候比较version



>  Redis的监视测试

正常执行成功：

```bash
set money 100
set out 0
watch money								#监视money对象
multi									#事务正常结束，数据期间没有发生变动，这个时候就正常执行成功
decrby money 20
incrby out 20
exec
```



测试多线程修改至，监视失败，使用watch充当Redis的乐观锁：

```bash
watch money
decrby money 10
incrby out 10
exec									#在exec之前，在另一个线程修改money的值,就会导致执行失败
（nil）
```



如果获取失败，获取最新的值就好：

```bash
unwatch					#如果事务执行失败，就先解锁
watch money				#获取最新的值，再次监视	select watch

multi
decrby money 10
incrby out 10
exec					# 比对监视的值是否发生了变化，如果没有变化，那么可以执行成功；如果变化了，就执行失败
```







# Jedis

我们使用java来操作Redis

> 什么是Jedis，是Redis官方推荐的java连接开发工具，使用java操作Redis中间件！如果你要使用java操作Redis，那么一定要对Jedis十分的熟悉！

知其然并知其所以然

> 测试

1、 导入对应的依赖

```xml
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>3.2.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/com.alibaba/fastjson -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.62</version>
</dependency>
```

2、 编码测试

- 连接数据库
- 操作命令
- 断开连接

![image-20200720114508223](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200720114508223.png)

输出：

![image-20200720114517615](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200720114517615.png)



## 常用的API

String

List

Set

Hash

Zset

> 事务

 



# SpringBoot整合

SpringBoot 操作数据：spring-data jpa jdbc mongodb redis！

SpringData 也是和 SpringBoot 齐名的项目！

说明： 在 SpringBoot2.x 之后，原来使用的jedis 被替换为了 lettuce?



Jedis：采用的直连，多个线程操作的haunted，是不安全的，如果想要避免不安全，使用Jedis pool连接池，像BIO模式

lettuce：采用Netty，实例可以在多个线程中进行共享，不存在线程不安全的情况！可以减少线程数量，更像NIO模式



> 整合测试一下

1. 导入依赖

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-data-redis</artifactId>
   </dependency>
   ```

2. 配置连接

   ```xml
   spring.redis.host=127.0.0.1
   spring.redis.port=6379
   ```

3. 测试







































## Redis.conf详解

启动的时候，就通过配置文件来启动

> 单元

![image-20200721102314577](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200721102314577.png)

- 配置文件unit单位对大小写不敏感

> 包含

![image-20200721102339350](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200721102339350.png)

- 好比学习Spring，Import，把多个配置文件配置过来

> 网络

```bash
bind 127.0.0.1				#绑定IP
protected-mode yes			#保护模式
port 6379					#端口设置
```



> 通用GENERAL

```bash
daemonize yes			#以守护进程的方式运行，默认是no，我们需要自己开启为yes

pidfile /var/run/redis_6379.pid		#如果以后台方式运行，我们就需要指定一个PID进程文件

# 日志
# Specify the server verbosity level.
# This can be one of:
# debug (a lot of information, useful for development/testing)
# verbose (many rarely useful info, but not a mess like the debug level)
# notice (moderately verbose, what you want in production probably)
# warning (only very important / critical messages are logged)
loglevel notice

logfile ""				#日志的文件位置名

databases 16			#数据库的数量

always-show-logo yes	#是否显示logo

```

> 快照

持久化，在规定的时间内，执行了多少次操作，则会持久化到文件 .rdb .aof

redis是内存数据库，如果没有持久化，那么数据断电即失

```bash
save 900 1						#如果900秒内，至少有一个key进行过修改，就进行持久化操作
save 300 10						#如果300秒内，至少有10个key进行过修改，就进行持久化操作
save 60 10000					#如果60秒内，至少10000个key进行了修改，就进行持久化操作


stop-writes-on-bgsave-error yes		#持久化如果出错，是否还需要继续工作

rdbcompression yes				#是否压缩rdb文件，需要消耗一些CPU资源

rdbchecksum yes  				#保存rdb文件的时候，进行错误的检查校验


dir ./							#rdb文件的保存目录

```

> REPLICATION复制，主从复制再写





> SECURITY安全

- 可以在这里设置redis的密码，默认是没有密码的

```bash
config set requirepass "123456"			# 设置密码为123456

#所有的命令都没有了权限

auth 123456							# 使用密码登录
```

> 限制 CLIENTS

```bash
maxclients 10000 			# 设置能连接上redis的最大客户端的数量

maxmemory <bytes>			# redis配置最大的内存容量

maxmemory-policy noeviction		# 内存到达上限之后的处理策略
	1、volatile-lru：只对设置了过期时间的key进行LRU（默认值） 
    2、allkeys-lru ： 删除lru算法的key   
    3、volatile-random：随机删除即将过期key   
    4、allkeys-random：随机删除   
    5、volatile-ttl ： 删除即将过期的   
    6、noeviction ： 永不过期，返回错误
```

> APPEND ONLY 模式 aof 配置

```bash
appendonly no			# 默认是不开启aof模式的，默认是使用rdb方式持久化的，在大部分所有的情况下，rdb完全够用

appendfilename "appendonly.aof"				# 持久化的文件的名字


# appendfsync always			# 每次修改都会 sync，消耗性能
appendfsync everysec			# 每秒执行一次 sync，可能会丢失这1s的数据
# appendfsync no				# 不执行sync， 这个时候操作系统自己同步数据，速度是最快的。不同步
```





# Redis持久化

面试和工作，持久化都是重点！！！

Redis 是内存数据库，如果不将内存中的数据库状态保存到磁盘，那么一旦服务器进程退出，服务器中的数据库状态也会消失，所以Redis提供了持久化功能

## RDB（Redis DataBase）

> 什么是RDB

在主从复制中，rdb就是备用了，从机上面

在指定的时间间隔内，将内存中的数据集快照写入磁盘，也就是行话讲的Snapshot快照，它恢复时是将快照文件直接读到内存里

Redis会单独创建（fork）一个子进程来进行持久化，会先将数据写入到一个临时文件中，待持久化过程都结束了，再用这个临时文件替换上次持久化好的文件。整个过程中，主进程是不进行任何IO操作的，这就确保了极高的性能，如果需要进行大规模数据的恢复，但对于数据恢复的完整性不是非常敏感，那RDB方式要比AOF方式更加的搞笑。RDB的缺点是最后一次持久化后的数据可能丢失。我们默认的就是RDB，一般情况下不需要修改这个配置！

有时候在生产环境我们会将这个文件进行备份

**rdb保存的文件是dump.rdb**

![image-20200721112421261](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200721112421261.png)



> 触发机制

1. save的规则满足的情况下，会自动出发rdb规则
2. 执行flushall命令，也会出发我们的rdb规则
3. 退出redis，也会产生rdb文件

备份就会自动生成一个.rdb文件

![image-20200721114543494](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200721114543494.png)



> 如何恢复rdb文件

1. 只需要将rdb文件放在我们redis启动目录就可以了，redis启动的时候会自动检查dump.rdb文件，恢复其中的数据！

2. 查看需要存在的位置

   ```bash
   127.0.0.1:6379> config get dir
   1) "dir"
   2) "F:\\Environment\\Redis"		#如果在这个目录下存在dump.rdb文件，启动就会自动恢复其中的数据
   ```

> 几乎就他自己默认的配置就够用了，但是我们还是需要去学习

**优点**：

1.  适合大规模的数据恢复！dump.rdb
2. 对数据的完整性要求不高

**缺点**：

1. 需要一定的时间间隔进行操作！如果redis意外宕机了，这个最后一次修改数据的就没有了！
2. fork进程的时候，会占用一定的内存空间！！





## AOF（Append Only File）

将我们的所有命令都记录下来，history，恢复的时候就把这个文件全部再执行一遍

> 是什么





以日志的形式来记录每个写操作，将Redis执行过的所有指令记录下来（读操作不记录），只许追加文件但不可以该文件，redis启动之初会读取该文件重新构建数据，换言之，redis重启的话就根据日志文件的内容将写指令从前到后执行一次，以完成数据的恢复工作

**Aof保存的是appendonly.aof文件**

> appendonly.aof

![image-20200721143030417](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200721143030417.png)

默认是不开启的，我们需要手动进行配置。只需要将appendonly改为yes就开启了aof、

**没有aof文件：**

```bash
config set appendonly yes
config set save ""
```



重启，redis就可以生效了



如果aof文件有错误，redis是启动不起来的，我们需要修复这个aof文件，redis给我们提供了一个工具，`redis-check-aof`

![image-20200721153327397](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200721153327397.png)

```bash
redis-check-aof --fix appendonly.aof		#修复，删除错误
```

> 重写规则说明

aof 默认是文件的无限追加，文件越来越大

如果aof文件大于64m，太大了。fork一个新的进程来讲我们的文件进行重写！



> 优点和缺点

优点：

1. 每一次修改都同步，文件的完整会更加好！
2. 每秒同步一次，可能会丢失一秒的数据
3. 从不同步，效率最高

缺点：

1. 相对于数据文件来说，aof远远大于rdb，修复的速度比rdb慢
2. 运行效率比rdb慢，所以我们redis默认的配置就是rdb持久化！





**扩展：**

1. rdb持久化方式能够在指定的时间间隔内对你的数据进行快照存储
2. aof持久化方式记录每次对服务器写的操作，当服务器重启的时候会重新执行这些命令来恢复原始的数据，aof命令以redis协议追加保存每次写的操作到文件末尾，redis还能对aof文件进行后台重写，使得aof文件的体积不至于过大
3. 只做缓存，如果你只希望你的数据在服务器运行的时候存在，你也可以不吃用任何持久化
4. 同时开启两种持久化方式
   - 在这种情况下，当redis重启的时候会优先载入aof文件来恢复原始的数据，因为在通常情况下aof文件保存的数据集要比rdb文件保存的数据集要完整
   - rdb的数据不实时，同时使用两者是服务器重启也只会找aof文件，那要不要只是用aof文件？作者建议不要，因为rdb更适合用于备份数据库（aof在不断变化不好备份）快速重启，而且不会有aof可能潜在的bug，留着作为一个玩意的手段
5. 性能建议
   - 因为rdb文件只用作后备用图，建议只在Slave上持久化rdb文件，而且只要15分钟备份一次就够了，只保留save 900 1这条规则
   - 如果enable aof，好处实在在最恶劣的情况下也只会丢失不超过两秒数据，启动脚本较简单只load自己的aof文件就可以了，代价一是带来了持续的IO，而是aof rewrite的最后将rewrite过车用中产生的新数据写到新文件造成的阻塞几乎是不可避免的，只要硬盘许可，应该尽量减少aof rewrite的频率，aof重写的基础大小默认值64m太小了，可以设到5G以上，默认超过原大小100%大小重写可以改到适当的数据
   - 如果不enable aof，仅靠Master-Slave Repllcation实现高可用性也可以，能省掉一大笔IO，也减少了rewrite时带来的系统波动，代价是如果Master/Slave同时倒掉【断电】，会丢失十几分钟的数据，启动脚本也要比较两个Master/Slave中的RDB文件，载入较新的那个，微博就是这种架构









# Redis发布订阅

通信 	队列 			发送者，订阅者

Redis发布订阅（pub/sub）是一种**消息通信模式**：发送者（pub）发送消息，订阅者（sub）接受消息。

redis客户端可以订阅任意数量的频道

订阅/发布消息图：

第一：消息发送者。第二：频道。第三：消息订阅者。

> 命令

这些命令被广泛用于构建即时通信应用，比如网络聊天室（chatroom）和实时广播，实时提醒等

**订阅端：**

```bash
127.0.0.1:6379> subscribe zhangsan			#订阅一个频道
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "zhangsan"
3) (integer) 1

# 等待读取推送的消息
1) "message"		#消息
2) "zhangsan"		#哪个频道的消息
3) "hello"			#消息的具体内容
# 等待读取推送的消息
1) "message"
2) "zhangsan"
3) "hello zhangsan"
```

**发布端：**

```bash
127.0.0.1:6379> publish zhangsan "hello"	#发布者发布消息到频道
(integer) 1
127.0.0.1:6379> publish zhangsan "hello zhangsan"	#发布者发布消息到频道
(integer) 1
```



使用场景：

1. 实时消息系统
2. 事实聊天！（频道当作聊天室，将信息回显给所有人即可！）
3. 订阅，关注系统

稍微复杂的场景我们就会使用消息中间件MQ（）





# Redis主从复制

## 概念

​		主从复制，是指将一台Redis服务器的数据，复制到其他的Redis服务器。前者称为主节点(master)，后者称为从节点(slave)，数据的复制是单向的，只能由主节点到从节点。

​		**默认情况下，每台Redis服务器都是主节点**；且**一个主节点可以有多个从节点**(或没有从节点)，但一个从节点只能有一个主节点。

## 作用

1. **数据冗余**：主从复制实现了数据的热备份，是持久化之外的一种数据冗余方式。
2. **故障恢复**：当主节点出现问题时，可以由从节点提供服务，实现快速的故障恢复；实际上是一种服务的冗余。
3. **负载均衡**：在主从复制的基础上，配合读写分离，可以由主节点提供写服务，由从节点提供读服务（即写Redis数据时应用连接主节点，读Redis数据时应用连接从节点），分担服务器负载；尤其是在写少读多的场景下，通过多个从节点分担读负载，可以大大提高Redis服务器的并发量。
4. **读写分离**：可以用于实现读写分离，主库写、从库读，读写分离不仅可以提高服务器的负载能力，同时可根据需求的变化，改变从库的数量。
5. **高可用基石**：除了上述作用以外，主从复制还是哨兵和集群能够实施的基础，因此说主从复制是Redis高可用的基础。



一般来说,要将redis运用在工程项目中,只使用一台redis是万万不能的(宕机，一主二从),原因如下

1.从结构上,单个redis服务器会发送单点故障,并且一台服务器需要处理所有的请求负载,压力太大

2.从容量上,单个服务器内存容量有限,就算一台redis服务器的内存容量为256g,也不能将所有内存用作redis存储内存,**一般来说redis最大使用内存不应该超过20g.**（切换成集群）



主从复制,读写分离.80%的情况下都是在进行读操作,减缓服务器的压力.最低配一主二从

在公司中,主从复制是必须使用的,因为在真实的项目中不可能单机使用redis



## 环境配置

只配置从库，不用配置主库！

```BASH
127.0.0.1:6379> info replication//查看当前库的信息
# Replication
role:master    //角色,当前是主机
connected_slaves:0  //从机数目
master_replid:33a4b2e54594f78449412f27d4222d5f887633ec
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0
```

复制了3个配置文件,修改了对应的信息

1.端口

2.pid名字

3.log文件名字

4.dump.rdb名字

开启服务：

```bash
./redis-server cyqconfig/redis79.conf
./redis-server cyqconfig/redis80.conf
./redis-server cyqconfig/redis81.conf
```

查看：

![image-20200722092746404](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722092746404.png)





# 一主二从

默认情况下，每台redis服务器都是主节点：一般情况下只用配置从机就好了！

![image-20200722101902520](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722101902520.png)

一主（79）二从（80、81）

```bash
127.0.0.1:6380> slaveof 127.0.0.1 6379		#slaveof host port
OK
127.0.0.1:6380> info replication
# Replication
role:slave					#当前角色
master_host:127.0.0.1
master_port:6379
master_link_status:down
master_last_io_seconds_ago:-1
master_sync_in_progress:0
slave_repl_offset:1
master_link_down_since_seconds:1594024869
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:d24109db1cef5dc56a98e5f202e63728ba1aa8fe
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0
```

![image-20200722095017423](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722095017423.png)



真正的主从配置应该在配置文件中配置，这样才是永久的，我们用命令是临时的

换成对应的ip和port

如果有密码，写上密码

![image-20200722095448488](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722095448488.png)



> 细节

主机可以写，从机不能写只能读！

主机中的所有信息和数据，都会自动被从机保存



主机可以写

![image-20200722095933803](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722095933803.png)



从机写不了：

![image-20200722095904051](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722095904051.png)



测试：

主机断开连接，从机依旧连接到主机，但是没有了‘写’操作

主机如果重新连接，从机依旧可以重新获得主机‘写’的信息



如果是使用命令行来配置的主从，这个时候如果重连，就会变为主机。**只要变为从机，立马就会从主机中获取值**

> 复制原理

slave启动成功连接master后会发送一个sync同步命令

master接到命令,启动后台的存盘进程,同时收集所有接收到的用于修改数据集命令,在后台进程执行完毕后,master将传送整个数据文件到slave,并完成一次完全同步.

全量复制:而slave服务在接收到数据库文件数据后,将其存盘并加载到内存中.

增量复制:master继续将新的所有收集到的修改命令依次传给slave,完成同步

但是只要是重新连接master,一次完全同步(全量复制)将被自动执行!我们的数据一定可以在从机中看到!



> 人体蜈蚣：

上一个M链接下一个S





![image-20200722101838162](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722101838162.png)

如果79宕机了,我们手动操作使用命令SLAVEOF no one将80设置为新主机,其他的节点就可以手动连接到这个新的主机节点.



如果第一个M没了，能不能

> 谋权篡位

如果主机断开了，我们可以使用`slaveof no one`让自己变成主机！其他的从机就可以手动连接到最新的这个主机

如果这个时候老主机修复了，那就需要重新连接配置









# 哨兵模式：

> 概述：

主从切换技术的方法是，当主服务器宕机的时候，需要手动把从机配置成主机，需要人工干预，费时费力，还会造成一段时间内服务不可用，这不是一种推荐的方式，更多时候，我们优先考虑哨兵模式，Redis从2.8开始正式提供了Sentinel（哨兵）架构来解决这个问题



谋权篡位的自动版

能够后台监控主机是否故障，如果故障了，根据投票数**自动将从库转换为主库**。



哨兵是一种特殊的模式，首先Redis提供了哨兵的命令，哨兵是一个独立的进程，作为进程，它会度流行，其原理是**哨兵通过发送命令，等待redis服务器响应，从而监控运行的多个Redis实例**



但是一个哨兵进程对redis服务器进行监控，可能会出现问题，为此，我们可以使用多个哨兵进行监控，各个哨兵之间还会进行监控，这样就形成了多哨兵模式



> 测试

我们目前的状态是一主二从！



1. 配置哨兵配置文件

   sentinel.conf

   ```bash
   # sentinel monitor 被监控的名称 host 	port 1
    sentinel monitor myredis 127.0.0.1 6379 1
    sentinel auth-pass myredis 123456	#密码位123456,6379,6380,6381的配置文件都要设置连接主机的密码为123456
   ```

   后面的这个数字1，代表主机挂了，slave投票看让谁接替成为主机，票数最多的，就会成为主机！

2. 启动哨兵！

如果Master节点断开，这个时候就会在从机中随机选择一个服务器！（这里面有一个投票算法！）



哨兵日志：

![image-20200722105153674](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722105153674.png)

如果此时主机回来了，只能归并到新的主机下，当做从机。这就是哨兵模式的规则！

> 哨兵模式

优点：

	1. 哨兵集群，基于主从复制模式，所有的主从配置优点，它全有
 	2. 主从可以切换，故障可以转移，系统的可用性就会更好
 	3. 哨兵模式就是主从模式的升级，从手动到自动，更加健壮

缺点：

1. redis不好在线扩容，集群的容量一旦到达上限，在线扩容就十分麻烦
2. 实现哨兵模式的配置其实是很麻烦的，里面有很多选择

> 哨兵模式的全部配置













springboot整合

一主二从三哨兵

1. 改服务器的配置文件
   - 不同的端口
   - rdb
   - pid
   - log
   - 设置主机
2. 改sentinel
   - 修改端口
   - 后台启动
   - 保护
   - sentinel mon







一主二从三哨兵：https://blog.csdn.net/weixin_41070914/article/details/106904623



![image-20200722163322729](C:\Users\chenyeqing\AppData\Roaming\Typora\typora-user-images\image-20200722163322729.png)























# 报错：

```csharp
MISCONF Redis is configured to save RDB snapshots, but is currently not able to persist on disk. Commands that may modify the data set are disabled. Please check Redis logs for details about the error.
```

网上的基本解决方案都是：

**将配置项stop-writes-on-bgsave-error 设置为no，可以在redis命令行里配置，也可以在redis.conf配置文件里改。**

```css
127.0.0.1:6379> config set stop-writes-on-bgsave-error no
```