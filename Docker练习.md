> 作业：docker装一个nginx

```shell
# 1. 搜索镜像	search
# 2. 下载镜像	pull
# 3. 运行测试
# -d 后台运行	--name 给容器命名	-p 宿主机端口:容器内部端口
[root@izbp19vpr16ix5hjusy95nz home]# docker run -d --name nginx01 -p 3344:80 nginx
ae2a7692f6b4f4a102ffba7dc04a86871853b0f592c03b96940b27ef7f3021e5
[root@izbp19vpr16ix5hjusy95nz home]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
ae2a7692f6b4        nginx               "/docker-entrypoint.…"   4 seconds ago       Up 3 seconds        0.0.0.0:3344->80/tcp   nginx01
[root@izbp19vpr16ix5hjusy95nz home]# curl localhost:3344

# 进入容器
[root@izbp19vpr16ix5hjusy95nz home]# docker exec -it nginx01 /bin/bash
root@ae2a7692f6b4:/# ls
bin   dev                  docker-entrypoint.sh  home  lib64  mnt  proc  run   srv  tmp  var
boot  docker-entrypoint.d  etc                   lib   media  opt  root  sbin  sys  usr
root@ae2a7692f6b4:/# whereis nginx
nginx: /usr/sbin/nginx /usr/lib/nginx /etc/nginx /usr/share/nginx
root@ae2a7692f6b4:/# cd etc/nginx
root@ae2a7692f6b4:/etc/nginx# ls
conf.d  fastcgi_params  koi-utf  koi-win  mime.types  modules  nginx.conf  scgi_params  uwsgi_params  win-utf

```

思考：我们每次改动nginx配置文件，都需要进入容器内部？十分的麻烦，我要是可以再容器外部提供一个映射路径，达到在容器修改文件，容器内不就可以自动修改？		数据卷！



> 作业：docker装一个tomcat

```shell
# 官方的用法
docker run -it --rm tomcat:9.0

# 我们之前的启动都是后台，停止了容器之后，容器还是可以查到    docker run -it --rm,一般用来测试，用完就删除

# 下载再启动
docker pull tomcat

# 启动运行
docker run -d -p 3355:8080 --name tomcat01 tomcat

# 测试访问没问题(404)之后进入容器
[root@izbp19vpr16ix5hjusy95nz home]# docker run -d -p 3355:8080 --name tomcat01 tomcat
16881df025aa34a384df940c9853c3b1667f85fcb305a2acef9a1ac12d836735
[root@izbp19vpr16ix5hjusy95nz home]# docker exec -it tomcat01 /bin/bash

# 发现问题：1、linux命令少了。2、没有webapps。
# 阿里云镜像的原因，默认是最小的镜像，所有不必要的都剔除掉。保证最小可运行的环境

```

思考：我们以后要部署项目，如果每次都要进入容器是不是十分麻烦？我要是可以在容器外部提供一个映射路径，webaps，我们在外部防止项目，就自动同步到内部就好了





> 作业：部署es + kibana

```shell
# es 暴露的端口很多！
# es 十分的耗内存
# es 的数据一般需要防止到安全目录！挂载

docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.6.2

# 启动了Linux就卡住了，docker stats 查看 cpu的状态

# es 是十分耗内存的		1核2G

# 查看 docket stats

# 赶紧关闭，增加内存的限制
```

```shell
# 增加内存的限制，修改配置文件 -e 环境配置修改
docker run -d --name elasticsearch02 -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e ES_JAVA_OPTS="-Xms64m -Xmx512m" elasticsearch:7.6.2

[root@izbp19vpr16ix5hjusy95nz ~]# curl localhost:9200
{
  "name" : "32ef49a263c6",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "CjypKwhAQTSCN3sS8nSiiw",
  "version" : {
    "number" : "7.6.2",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "ef48eb35cf30adf4db14086e8aabd07ef6fb113f",
    "build_date" : "2020-03-26T06:34:37.794943Z",
    "build_snapshot" : false,
    "lucene_version" : "8.4.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}

```

作业：使用 kibana 连接 elasticsearch ？思考网络如何连接过去





## 可视化

- portainer（先用这个）



- Rancher(CI/CD再用)



什么是portainer？

Docker图形化界面管理工具！提供一个后台面板供我们操作！

```shell
docker run -d -p 8088:9000 \
--restart=always -v /var/run/docker.sock:/var/run/docker.sock --privileged=true portainer/portainer
```













